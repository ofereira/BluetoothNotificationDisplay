package com.inspiredsolutions.app.bndmaster;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    EditText text;
    Button sendButton;
    Button searchButton;
    Button unpairButton;
    ProgressBar progressBar;
    TextView paired;

    private BluetoothAdapter bluetooth;
    private BluetoothSocket socket;
    String aString="Bluetooth-Notification-Display";
    private UUID uuid = UUID.nameUUIDFromBytes(aString.getBytes());
    private ArrayList<BluetoothDevice> foundDevices = new ArrayList<>();
    private ArrayList<String> nameDevices = new ArrayList<>();
    private ArrayAdapter<String> aa;
    private ListView list;
    private BluetoothDevice pairedDevice;
    private String error;

    BroadcastReceiver discoveryResult = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                progressBar.setVisibility(View.VISIBLE);

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                progressBar.setVisibility(View.GONE);

            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //bluetooth device found
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                foundDevices.add(device);
                nameDevices.add(device.getName());
                aa.notifyDataSetChanged();

            }else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)){
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                pairedDevice = device;
                setPaired("Paired with"+device.getName());
            }else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
                setPaired("");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }

        text = (EditText)findViewById(R.id.text);
        sendButton = (Button)findViewById(R.id.btn_send);
        searchButton = (Button)findViewById(R.id.btn_search);
        unpairButton = (Button)findViewById(R.id.btn_unpair);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        paired = (TextView) findViewById(R.id.paired);

        bluetooth = BluetoothAdapter.getDefaultAdapter();

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage(socket, text.getText().toString());
            }
        });

        unpairButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unpairDevice(pairedDevice);
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                IntentFilter filter = new IntentFilter();

                filter.addAction(BluetoothDevice.ACTION_FOUND);
                filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
                filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
                filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
                filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);

                registerReceiver(discoveryResult, filter);

                if (bluetooth == null) {
                    Toast.makeText(MainActivity.this, "Device do not support bluetooth", Toast.LENGTH_SHORT).show();
                } else if (!bluetooth.isEnabled()) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog.setMessage("Bluetooth need to be enabled to work.\nGo to settings and enabled it.");
                    dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intentOpenBluetoothSettings = new Intent();
                            intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                            startActivity(intentOpenBluetoothSettings);
                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.create().show();
                }else {
                    if (!bluetooth.isDiscovering()) {
                        nameDevices.clear();
                        foundDevices.clear();
                        bluetooth.startDiscovery();
                    }
                }
            }
        });

        setupListView();

    }

    void setPaired(String s){
        paired.setText(s);
    }

    private void unpairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("removeBond", (Class[]) null);
            method.invoke(device, (Object[]) null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupListView() {
        aa = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                nameDevices);
        list = (ListView)findViewById(R.id.list_discovered);
        list.setAdapter(aa);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view,
                                    int index, long arg3) {
                AsyncTask<Integer, Void, Void> connectTask =
                        new AsyncTask<Integer, Void, Void>() {
                            @Override
                            protected Void doInBackground(Integer... params) {
                                try {
                                    BluetoothDevice device = foundDevices.get(params[0]);
                                    socket = device.createRfcommSocketToServiceRecord(uuid);
                                    socket.connect();
                                } catch (IOException e) {
                                    Log.d("BLUETOOTH_CLIENT", e.getMessage());
                                }
                                return null;
                            }
                            @Override
                            protected void onPostExecute(Void result) {
                                if (!socket.isConnected()){
                                    Toast.makeText(MainActivity.this, "Connection not allowed. Install BNDSlave to connect.", Toast.LENGTH_LONG).show();
                                }
                            }
                        };
                connectTask.execute(index);
            }
        });
    }

    private void sendMessage(BluetoothSocket socket, String msg) {
        OutputStream outStream;
        try {
            outStream = socket.getOutputStream();
            byte[] byteString = (msg + " ").getBytes();
//            stringAsBytes[byteString.length-1] = 0;
            outStream.write(byteString);
        } catch (IOException e) {
            Log.d("BLUETOOTH_COMMS", e.getMessage());
        }
    }

    public class ConnectTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            try {
                BluetoothDevice device = foundDevices.get(params[0]);
                socket = device.createRfcommSocketToServiceRecord(uuid);
                socket.connect();
            } catch (IOException e) {
                error = e.getMessage();
                Log.d("BLUETOOTH_CLIENT", e.getMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            if (error != null){
                Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
//                                    "Connection not allowed. Install BNDSlave to connect."
                error = null;
            }
        }
    }

}
