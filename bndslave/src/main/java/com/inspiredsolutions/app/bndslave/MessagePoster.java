package com.inspiredsolutions.app.bndslave;

import android.widget.TextView;

/**
 * Created by Sergio on 22/02/2018.
 */

public class MessagePoster implements Runnable {
    private TextView textView;
    private String message;
    public MessagePoster(TextView textView, String message) {
        this.textView = textView;
        this.message = message;
    }
    public void run() {
        textView.setText(message);
    }
}