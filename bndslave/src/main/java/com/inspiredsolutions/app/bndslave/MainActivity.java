package com.inspiredsolutions.app.bndslave;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static int DISCOVERY_REQUEST = 1;
    private static int REQUEST_SETTING = 2;

    private BluetoothSocket socket;
    private BluetoothAdapter bluetooth;

    String aString="Bluetooth-Notification-Display";
    private UUID uuid = UUID.nameUUIDFromBytes(aString.getBytes());

    TextView textView;

    private Handler handler = new Handler();

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_ON:
                        intentDiscoverable();
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.tv_text);
        bluetooth = BluetoothAdapter.getDefaultAdapter();

        if (bluetooth == null) {
            Toast.makeText(MainActivity.this, "Device do not support bluetooth", Toast.LENGTH_SHORT).show();
        } else if (!bluetooth.isEnabled()) {
            /*AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setMessage("Bluetooth need to be enabled to work.\nGo to settings and enabled it.");
            dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intentOpenBluetoothSettings = new Intent();
                    intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                    startActivityForResult(intentOpenBluetoothSettings, REQUEST_SETTING);
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.create().show();*/
            setBluetooth(true);
        }else{
            intentDiscoverable();
        }

        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
    }

    public void setBluetooth(boolean enable) {
        boolean isEnabled = bluetooth.isEnabled();
        if (enable && !isEnabled) {
            bluetooth.enable();
        }
        else if(!enable && isEnabled) {
            bluetooth.disable();
        }
    }

    void intentDiscoverable(){
        Intent disc;
        disc = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
//        disc.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
        startActivityForResult(disc, DISCOVERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DISCOVERY_REQUEST) {
            Log.d("resultCode", String.valueOf(resultCode));
            boolean isDiscoverable = resultCode > 0;
            if (isDiscoverable) {
                String name = "BNDMaster";
                try {
                    final BluetoothServerSocket btserver = bluetooth.listenUsingRfcommWithServiceRecord(name, uuid);
                    AsyncTask<Integer, Void, BluetoothSocket> acceptThread =
                            new AsyncTask<Integer, Void, BluetoothSocket>() {
                                @Override
                                protected BluetoothSocket doInBackground(Integer... params) {

                                    try {
                                        socket = btserver.accept(params[0]*1000);
                                        return socket;
                                    } catch (IOException e) {
                                        Log.d("BLUETOOTH", e.getMessage());
                                    }

                                    return null;
                                }
                                @Override
                                protected void onPostExecute(BluetoothSocket result) {
//                                    setupListView();
                                    BluetoothSocketListener bsl = new BluetoothSocketListener(socket,
                                            handler, textView);
                                    Thread messageListener = new Thread(bsl);
                                    messageListener.start();

                                }
                            };
                    acceptThread.execute(resultCode);
                } catch (IOException e) {
                    Log.d("BLUETOOTH", e.getMessage());
                }
            }
        }else if(requestCode == REQUEST_SETTING){
           intentDiscoverable();
        }
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);
        super.onStop();
    }

    static class AcceptThread extends AsyncTask<Integer, Void, BluetoothSocket>{

        BluetoothServerSocket btserver;
        Context context;

        void setData(Context context, BluetoothServerSocket btserver){
            this.context = context;
            this.btserver = btserver;
        }

        @Override
        protected BluetoothSocket doInBackground(Integer... params) {

            try {
                ((MainActivity) context).socket = btserver.accept(params[0]*1000);
                return  ((MainActivity) context).socket;
            } catch (IOException e) {
                Log.d("BLUETOOTH", e.getMessage());
            }

            return null;
        }
        @Override
        protected void onPostExecute(BluetoothSocket result) {
            BluetoothSocketListener bsl = new BluetoothSocketListener( ((MainActivity) context).socket,
                    ((MainActivity) context).handler,  ((MainActivity) context).textView);
            Thread messageListener = new Thread(bsl);
            messageListener.start();

        }
    }
}
